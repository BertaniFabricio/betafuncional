﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//METODO PARA INTANCIAR PARTICULA DE METAL
	public class hitMetal : MonoBehaviour
	{
		public GameObject metalParticle;

		public void rayHitMetal(RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (metalParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
		}
	}
}