﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class MedicineScript : MonoBehaviour
	{
		public AudioClip medic;

		private Transform _playerTrans = null;	

		PlayerScript _playerScript;

		void Start ()
		{
			//Busca el transform del player para medir la cercania
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			_playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript> ();
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.E))
			{
				//Compara la distancia entre el player y el objeto
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					if (_playerScript.health < 100)
					{
						/*si la distancia es menor a 2.5 y aprieta la tecla E
						 *llama al metodo newMagazines del arma y destruye el objeto
						 *como si lo recogiera*/
						AudioSource.PlayClipAtPoint (medic, transform.position);
						_playerScript.Medicine ();
						Destroy (gameObject);
					}
				}
			}
		}
	}
}