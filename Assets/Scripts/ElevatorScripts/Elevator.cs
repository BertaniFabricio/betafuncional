﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//ESTE SCRIPT ES MAS POO
	public class Elevator : MonoBehaviour
	{
		//ESTE ES EL MASTER DEL ASCENSOR
		//ACA SE SETEAN TODOS LOS ESTADOS, METODOS Y BOOLES

		public GameObject elevator_A;
		public GameObject PuertaPlantaBaja;
		public GameObject PuertaPlantaAlta;
		public GameObject PuertaMorgue;

		public AudioClip ElevatorDoorOpen;
		public AudioClip ElevatorDoorClose;
		public AudioClip ElevatorSound;

		public bool PlantaBaja;
		public bool PrimerPiso;
		public bool Morgue;

		//variables protegidas que solo pueden ser accedidas
		//por las clases heredadas
		protected Animation elevatorAnim;
		protected Animation PuertaPB;
		protected Animation PuertaPA;
		protected Animation PuertaME;


		//LOS METODOS PRIVADOS NO PUEDEN SER ACCEDIDOS
		//POR LAS CLASES HEREDADAS
		//SOLO PUEDEN ACCEDER METODOS PUBLICOS Y PROTEGIDOS
		public void Awake()
		{
			elevatorAnim = elevator_A.GetComponent<Animation> ();
			PuertaPB = PuertaPlantaBaja.GetComponent<Animation> ();
			PuertaPA = PuertaPlantaAlta.GetComponent<Animation> ();
			PuertaME = PuertaMorgue.GetComponent<Animation> ();
		}

		public void Start()
		{
			PlantaBaja = true;
			PrimerPiso = false;
			Morgue = false;
		}

		public void boolPBTrue()
		{
			PlantaBaja = true;
		}

		public void boolPBFalse()
		{
			PlantaBaja = false;
		}

		public void boolPPTrue()
		{
			PrimerPiso = true;
		}

		public void boolPPFalse()
		{
			PrimerPiso = false;
		}

		public void boolMTrue()
		{
			Morgue = true;
		}

		public void boolMFalse()
		{
			Morgue = false;
		}
	}
}