﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	//SCRIPT PARA EL RELOJ, NO CAMBIO
	public class Timer : MonoBehaviour
	{
		public Text timerText;

		private float _secondsCount;
		private int _minuteCount;
		private int _hourCount;

		void Start()
		{
			_secondsCount = 59.9f;
			_minuteCount = 16;
			_hourCount = 2;
		}

		public void UpdateTimerUI()
		{
			_secondsCount -= Time.deltaTime;
			timerText.text = "0" + _hourCount + " : " + _minuteCount + " : " + (int)_secondsCount;
			if (_secondsCount < 10.0f)
			{
				timerText.text = "0" + _hourCount + " : " + _minuteCount + " : 0" + (int)_secondsCount;
			}
			if (_minuteCount < 10)
			{
				timerText.text = "0" + _hourCount + " : 0" + _minuteCount + " : " + (int)_secondsCount;
			}

			if (_secondsCount <= 0.0f)
			{
				_minuteCount--;
				_secondsCount = 59.9f;
			}
			if (_minuteCount <= 0)
			{
				_hourCount--;
				_minuteCount = 59;
			}
		}

		void Update()
		{
			UpdateTimerUI ();
		}
	}
}