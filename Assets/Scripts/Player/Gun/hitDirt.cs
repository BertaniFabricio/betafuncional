﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//METODO PARA INTANCIAR PARTICULA DE TIERRA
	public class hitDirt : MonoBehaviour
	{
		public GameObject dirtParticle;

		public void rayHitDirty(RaycastHit hit)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (dirtParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
		}
	}
}