﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	/*ESTE ES EL GAME MANAGER DE LOS CONTADORES
	 *LLEVA EL CONTEO DEL PUNTAJE
	 *DE LA CANTIDAD DE ENEMIGOS ELIMINADOS
	 *Y DE LAS LLAVES RECOGIDAS 
	 *PARA MEJOR CONTROL Y FACIL ACCESO */
	public class GameManagerCounters : MonoBehaviour
	{
		public int enemiesCount;
		public int scoreValue;
		public int keysValue;

		public void enemyDead()
		{
			enemiesCount++;
			scoreValue = scoreValue + 10;
		}

		public void KeyFound()
		{
			keysValue++;
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.M))
			{
				enemiesCount = 28;
				scoreValue = 128;
				keysValue = 3;
			}
		}
	}
}