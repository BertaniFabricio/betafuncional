﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	/*SCRIPT PARA ACTIVAR LOS CARTELITOS
	 * DEL TUTORIAL*/
	public class tutorialScript : MonoBehaviour
	{
		SphereCollider col;
		Canvas messagge;

		void Start()
		{
			messagge = GetComponent<Canvas> ();
			col = GetComponent<SphereCollider> ();
			messagge.enabled = false;
			col.enabled = true;
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				messagge.enabled = true;
			}
		}

		void OnTriggerExit (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				messagge.enabled = false;
				col.enabled = false;
			}
		}
	}
}