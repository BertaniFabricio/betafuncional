﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class AmmoScript : MonoBehaviour
	{
		public AudioClip Bullets;

		private Transform _playerTrans = null;

		GunScript _playerGun;
		GameObject Gun;

		void Start()
		{
			//Busca el transform del player para medir la cercania
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			_playerGun = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.E))
			{
				//Compara la distancia entre el player y el objeto
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					/*si la distancia es menor a 2.5 y aprieta la tecla E
					 *llama al metodo newMagazines del arma y destruye el objeto
					 *como si lo recogiera*/
					AudioSource.PlayClipAtPoint (Bullets, transform.position);
					_playerGun.newMagazines ();
					Destroy (gameObject);
				}
			}
		}
	}
}