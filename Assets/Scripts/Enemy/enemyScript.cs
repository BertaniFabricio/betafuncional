﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ZombieHunting
{
	//ESTE SCRIPT ES MAS POO
	public class enemyScript : MonoBehaviour
	{
		//ESTE ES EL MASTER DEL ENEMIGO
		//ACA SE SETEAN TODOS LOS ESTADOS, METODOS Y BOOLES

		public int damageAmount;// <--
		//cantidad de daño que recibe el zombie, es publica para ser
		//seteada desde el inspector y sea más parametrizable con 
		//cada zombie en particular -->
		public int headshotAmount;

		public int health;
		public GameObject slider;
		public Slider enemySlider;
		 
		protected int _startingHealth = 100;
		protected NavMeshAgent _agent;
		protected Animator _animator;
		protected float deathTime;

		Transform _player;
		protected GameManagerCounters _gmcounter;

		void Awake()
		{
			_animator = GetComponent<Animator> ();
			_agent = GetComponent<NavMeshAgent> ();
		}

		void Start()
		{
			_gmcounter = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
			GameObject playerGameobject = GameObject.FindGameObjectWithTag ("Player");
			health = _startingHealth; //setea la vida inicial en 100
			_player = playerGameobject.transform;
			slider.SetActive (true);
		}

		void Walk()
		{
			if (health > 0)
			{
				//con esta sola linea de cogido el zombie
				//busca al player a traves del NavMesh
				//esquivando los objetos
				_agent.destination = _player.transform.position;
			}
		}

		public void takeDamage()
		{
			health -= damageAmount; //resta la cantidad de vida indicada
			enemySlider.value = health;
		}

		public void headShot()
		{
			health -= headshotAmount;
			enemySlider.value = health;
		}

		void isDead()
		{
			if (health <= 0)
			{
				slider.SetActive (false);
				_animator.SetBool ("isDead", true);
				_agent.Stop (); //<--
				//si el enemigo esta muerto deja de seguirlo
				//(el cuerpo queda quieto) -->

				startDeathTime ();
			}
		}

		void startDeathTime()
		{
			deathTime += Time.deltaTime;
			if (deathTime >= 2.0f) //tiempo para destruir el objeto
			{
				//metodo del game manager para contabilizar enemigos eliminados
				_gmcounter.enemyDead ();
				Destroy (gameObject);
			}
		}

		void Update()
		{
			Walk ();
			isDead ();
		}
	}
}