﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class FinalCreditsScript : MonoBehaviour
	{
		private bool _showCredits;
		void Start()
		{
			//Bloquea el movimiento del mouse y lo hace invisible
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			_showCredits = false;
			StartCoroutine (WaitCredits ());
		}

		public IEnumerator WaitCredits()
		{
			//Va cargando el menu de fondo mientras pasa la animacion de los creditos
			AsyncOperation async = SceneManager.LoadSceneAsync ("Menu", LoadSceneMode.Single);
			async.allowSceneActivation = false;
			//espera para cargar el menu
			yield return new WaitForSeconds (120.0f);
			_showCredits = true;
			if (_showCredits)
			{
				async.allowSceneActivation = true; //carga el menu
			}

			yield return null;
		}
	}
}