﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	/*ESTE SCRIPT MUESTRA LA PANTALLA
	 * DE FELICITACIONES CUANDO 
	 * SE GANA EL JUEGO */
	public class endGameScript : MonoBehaviour
	{
		public GameObject EndGameDisplay;

		void Start()
		{
			EndGameDisplay.SetActive (false);
		}

		IEnumerator waitForEnd()
		{
			EndGameDisplay.SetActive (true);
			yield return new WaitForSeconds (8.0f);
			SceneManager.LoadScene ("FinalCredits");

			yield return null;
		}

		void OnTriggerEnter (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				StartCoroutine(waitForEnd());
			}
		}
	}
}