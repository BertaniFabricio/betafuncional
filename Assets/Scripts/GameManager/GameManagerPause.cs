﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;//<--
//necesario para acceder al script del firstpersoncontroller -->

namespace ZombieHunting
{
	/*ESTE GAME MANAGER ES PARA
	 * MANEJAR LA PAUSA DEL JUEGO */
	public class GameManagerPause : MonoBehaviour
	{
		public GameObject hudDisplay;
		public GameObject pauseDisplay;

		private bool isPaused = false;

		FirstPersonController _fps;
		GunScript _gunScript;
		hudScript _hudScript;


		void Start()
		{
			Time.timeScale = 1; //tiempo normal
			_fps = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
			_gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
			_hudScript = GameObject.FindGameObjectWithTag ("hud").GetComponent<hudScript> ();
			pauseDisplay.SetActive (false); //oculta la pantalla de pausa
		}

		//para el boton de continuar
		public void onClickReturn()
		{
			Time.timeScale = 1;
			_fps.enabled = true;
			_gunScript.enabled = true;
			_hudScript.enabled = true;
			pauseDisplay.SetActive (false);
			hudDisplay.SetActive (true);
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = false;
			isPaused = false;
		}

		//para el boton de salir
		public void onClickExit()
		{
			Time.timeScale = 1;
			SceneManager.LoadScene ("Menu");
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.Escape))
			{
				isPaused = !isPaused;
				if (isPaused) //si está pausado...
				{
					Time.timeScale = 0; //tiempo congelado
					_gunScript.enabled = false; //<--
					/*desactiva el script del arma
					 * para que no pueda disparar -->*/

					_fps.enabled = false; //<--
					/*desactiva el firstpersoncontroller
					 * para que no pueda moverse -->*/

					_hudScript.enabled = false; //<--
					/*desactiva el script del hud 
					 * para que no tire errores jaja -->*/

					hudDisplay.SetActive (false); //<--
					//oculta el hud -->*/

					pauseDisplay.SetActive (true); //<--
					//muestra la pantalla de pausa

					//activa y muestra el cursor del mouse
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
				else if (!isPaused)
				{
					//vuelve todo a la normalidad 
					Time.timeScale = 1; //tiempo normal
					_fps.enabled = true; //fps activado
					_gunScript.enabled = true; //script arma activado
					_hudScript.enabled = true; //script hud activado
					pauseDisplay.SetActive (false); //pausa oculta
					hudDisplay.SetActive (true); //hud visible

					//cursor oculto
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = false;
				}
			}
		}
	}
}