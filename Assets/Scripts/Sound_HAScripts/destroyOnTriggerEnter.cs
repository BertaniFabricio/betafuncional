﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ZombieHunting
{
	public class destroyOnTriggerEnter : MonoBehaviour
	{
		public List<GameObject> objectsToDelete = new List<GameObject>();

		private bool _selfDestroy = true;

		void OnTriggerEnter( Collider other )
		{
			if( other.tag == "Player" )
			{
				foreach( GameObject o in objectsToDelete ) Destroy ( o );

				if( _selfDestroy ) Destroy ( this.gameObject );
			}
		}
	}
}