﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//FUEGUITO DEL ARMA WIIIIIIII
	public class muzzleFlash : MonoBehaviour
	{
		public float lifeTime;

		void Start()
		{
			
		}

		void OnEnable()
		{
			//hace aparecer el fueguito por determinado tiempo
			Invoke ("setAct", lifeTime);
		}

		void setAct()
		{
			gameObject.SetActive (false);
		}

		void Update()
		{
			//rota los planos del fueguito para mas realismo
			transform.localEulerAngles = new Vector3 (Random.Range (0, 359), 0, 0);
		}
	}
}