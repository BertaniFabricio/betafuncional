using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace ZombieHunting
{
	//NUEVO SCRIPT DEL ARMA
	public class GunScript : MonoBehaviour
	{
		public GameObject FPS_WeaponFire;
		public GameObject bulletHole;
		public GameObject muzzle1;
		public GameObject muzzle2;

		public AudioClip shotSound;
		public AudioClip emptySound;
		public AudioClip reloadSound;

		public int bulletsPerMagazine; //Para el hud y el disparo
		public int magazinesRemaining; //Para el hud y el disparo

		private Ray ray;
		private RaycastHit hit;
		private Animator _anim;
		private float _reloadTime = 1.5f;
		private float _shotTime = 0.52f;
		private float _nextShot;
		private float _nextReload;
		private int _ammoAmount = 9;
		private int _intShoot = Animator.StringToHash ("ShootOnce"); //hashea el trigger de la animacion
		private int _intReload = Animator.StringToHash ("ReloadOnce"); //hashea el trigger de la animacion

		void Awake()
		{
			_anim = FPS_WeaponFire.GetComponent<Animator> ();
		}

		void Start()
		{
			bulletsPerMagazine = _ammoAmount; //setea la cantida inicial de balas
			magazinesRemaining = 3; //setea la cantidad inicial de cargadores
			_nextShot = Time.time;
			_nextReload = Time.time;
		}

		//NUEVO DISPARO CON RAYCAST
		void shootRaycast()
		{
			Vector2 screenCenterPoint = new Vector2 (Screen.width / 2, Screen.height / 2);
			ray = Camera.main.ScreenPointToRay (screenCenterPoint);
			/*Con lo anterior pasa el ray exactamente por el centro de la pantalla
			 * a traves de la camara principal */

			if (Physics.Raycast (ray, out hit, Camera.main.farClipPlane))
			//dispara el raycast a la mayor distancia posible
			{
				Vector3 pos = hit.point; //la posicion donde pega el raycast
				Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);//la rotación sobre la normal para las particulas

				/*AHORA EL RAYCAST SOLAMENTE DISPARA Y COMPRUEBA
				 *A QUE TIPO DE OBJETO LE PEGO Y LE AVISA QUE
				 *LE PEGO, ES EL OBJETO AHORA QUE ACTUA EN
				 *CONSECUENCIA, REACCIONANDO DE DETERMINADO
				 *MODO CUANDO EL RAYCAST LE PEGA */
				if (hit.collider.gameObject.tag == "wood")
				{
					/*si le pego a algo con el tag de madera, ahora
					 *busca el script del objeto, en este caso 'hitWood',
					 *y le dice que active el metodo 'rayHitWood' pasandole
					 *como parametro el lugar donde le pego */
					hit.collider.gameObject.GetComponent<hitWood> ().rayHitWood (hit);
				}

				//LO MISMO QUE EL ANTERIOR CON TODOS LOS DEMAS

				if (hit.collider.gameObject.tag == "concrete")
				{
					hit.collider.gameObject.GetComponent<hitConcrete> ().rayHitConcrete (hit);
				}

				if (hit.collider.gameObject.tag == "metal")
				{
					hit.collider.gameObject.GetComponent<hitMetal> ().rayHitMetal (hit);
				}

				if (hit.collider.gameObject.tag == "dirt")
				{
					hit.collider.gameObject.GetComponent<hitDirt> ().rayHitDirty (hit);
				}

				if (hit.collider.gameObject.tag == "flesh")
				{
					hit.collider.gameObject.GetComponent<hitFlesh> ().rayHitFlesh (hit);
				}

				if (hit.collider.gameObject.tag == "Enemy")
				{
					hit.collider.gameObject.GetComponent<hitEnemy> ().rayHitEnemy (hit, false);
				}

				if (hit.collider.gameObject.tag == "Head")
				{
					hit.transform.parent.gameObject.GetComponent<hitEnemy> ().rayHitEnemy (hit, true);
				}

				//crea agujero de bala en cualquier objeto menos en los zombies
				if (hit.collider.gameObject.tag != "Enemy" && hit.collider.gameObject.tag != "Head")
				{
					GameObject hole;
					hole = Instantiate (bulletHole, pos, rot) as GameObject;
					hole.transform.parent = hit.collider.transform;
				}
			}
		}

		void Shooting()
		{
			//LE AGREGE UNA CONDICION PARA QUE EJECUTE SIEMPRE LA ANIMACION DE DISPARO CON EL CLICK
			//
			if (_nextShot <= Time.time)
			{
				_nextShot = Time.time + _shotTime;
				if (bulletsPerMagazine > 0) {
					GetComponent<AudioSource> ().clip = shotSound;
					GetComponent<AudioSource> ().Play ();
					shootRaycast ();
					_anim.SetTrigger (_intShoot);
					bulletsPerMagazine--;
					muzzle1.SetActive (true); //fueguito de disparo wiiiii
					muzzle2.SetActive (true); //fueguito de disparo wiiiii
				}
				else
				{
					GetComponent<AudioSource> ().clip = emptySound;
					GetComponent<AudioSource> ().Play ();
				}
			}
		}

		void Reload()
		{
			//ACA PARECIDO AL ANTERIOR, NO DISPARA HASTA QUE TERMINE DE RECARGAR
			if (_nextReload <= Time.time)
			{
				_nextReload = Time.time + _reloadTime;
				if (magazinesRemaining > 0)
				{
					if (bulletsPerMagazine <= 0)
					{
						GetComponent<AudioSource> ().clip = reloadSound;
						GetComponent<AudioSource> ().Play ();
						_anim.SetTrigger (_intReload);
						bulletsPerMagazine += _ammoAmount;
						magazinesRemaining--;
						//_nextReload = Time.time + _reloadTime;
						_nextShot = _nextReload;
					}
				}
			}
		}

		public void newMagazines()
		{
			//metodo para cuando junta las cajitas de balas
			magazinesRemaining = magazinesRemaining + 3;
		}

		void Update()
		{
			if (Input.GetButtonDown("Fire1"))
			{
				Shooting ();
			}

			if (Input.GetKeyDown (KeyCode.R))
			{
				Reload ();
			}
		}
	}
}