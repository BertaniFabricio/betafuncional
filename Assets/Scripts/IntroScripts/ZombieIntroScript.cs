﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//NO CAMBIO NADA
	public class ZombieIntroScript : MonoBehaviour
	{
		private Animator _animator;

		void Awake ()
		{
			_animator = GetComponent<Animator> ();
		}
		
		void Start ()
		{
			_animator.SetTrigger ("Intro");
		}
	}
}