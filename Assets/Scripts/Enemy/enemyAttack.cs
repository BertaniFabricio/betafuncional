﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class enemyAttack : enemyScript
	//CLASE HEREADA DE ENEMIGO, OBTIENE TODOS SUS METODOS
	//Y VARIABLES PUBLICAS O PROTEGIDAS
	{
		public int infligedDamage;// <--
		//cantidad de daño que produce el zombie, es publica para ser
		//seteada desde el inspector y sea más parametrizable con 
		//cada zombie en particular -->

		private float _timeBetweenAttacks = 1.0f;
		private float _timer;
		private bool _playerInRange;

		PlayerScript _playerScript;
		enemyScript _enemyScript;
		Animator _anim;

		void Awake()
		{
			_enemyScript = GetComponent<enemyScript> ();
			_anim = GetComponent<Animator> ();
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_agent = GetComponent<NavMeshAgent> ();
		}

		void OnTriggerEnter (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_playerInRange = true;
				_agent.Stop (); //<--
				//si el enemigo toca al player para de moverse
				//(también se puede setear desde el NavMesh
				//para que pare a cierta distancia) -->

				_anim.SetBool ("isPlayer", true);
			}
		}

		void OnTriggerExit (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_playerInRange = false;
				_agent.Resume (); //<--
				//si se aleja del trigger del player
				//lo vuelve a seguir -->

				_anim.SetBool ("isPlayer", false);
			}
		}

		void Attack()
		{
			_timer = 0.0f;

			if (_playerScript.health > 0)
			{
				_playerScript.takeDamage (infligedDamage); //le pasa el daño al metodo del player
				_playerScript.damaged = true; //setea en true para que salga la sangre en el hud del player
			}
		}

		void Update()
		{
			_timer += Time.deltaTime;

			if (_timer >= _timeBetweenAttacks && _playerInRange && _enemyScript.health > 0) {
				Attack ();
			}
			else
			{
				_playerScript.damaged = false; //setea en falso para que no muestre sangre en pantalla
			}
		}
	}
}