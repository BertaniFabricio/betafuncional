﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	/*UNICO CAMBIO ES QUE SE JUNTA
	 * LA LLAVE CUANDO SE ESTA CERCA
	 * Y SE APRIETA LA TECLA 'E' */
	public class KeysScript : MonoBehaviour
	{
		public AudioClip keySound;

		private Transform _playerTrans = null;

		GameManagerCounters _gmCounters;

		void Start()
		{
			_gmCounters = GameObject.FindGameObjectWithTag ("gmcounter").GetComponent<GameManagerCounters> ();
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
		}

		void Update()
		{
			if (Input.GetKeyDown (KeyCode.E))
			{
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				{
					AudioSource.PlayClipAtPoint (keySound, transform.position);
					_gmCounters.KeyFound ();
					Destroy (gameObject);
				}
			}
		}
	}
}