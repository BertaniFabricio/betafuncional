﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	//EL UNICO CAMBIO RESPECTO DEL ANTERIOR ES QUE SE DESACTIVA LA ANIMACION Y EL SONIDO
	// CUANDO EL PLAYER SALE DEL TRIGGER, QUE AHORA ES MAS GRANDE
	public class LastDoorScript : MonoBehaviour
	{
		public AudioClip LastDoor;

		private Animator _animator;

		void Awake()
		{
			_animator = GetComponent<Animator> ();
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_animator.SetBool ("Active", true);
				GetComponent<AudioSource> ().clip = LastDoor;
				GetComponent<AudioSource> ().Play ();
			}
		}

		void OnTriggerExit (Collider other)
		{
			if (other.gameObject.tag == "Player")
			{
				_animator.SetBool ("Active", false);
				GetComponent<AudioSource> ().Stop ();
			}
		}
	}
}