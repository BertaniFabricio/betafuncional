﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	/*NO CAMBIO NADA
	 *SOLO SE AGREGO SONIDO
	 *CUANDO PRENDE Y APAGAR
	 *LA LINTERNA */
	public class FlashLightScript : MonoBehaviour
	{
		public AudioClip flashLightOn;
		public AudioClip flashLightOff;
		private Light _flashLight;
		private bool _On = false;

		void Awake()
		{
			_flashLight = GetComponent<Light> ();
		}

		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.F))
			{
				_On = !_On;
				if (_On) {
					_flashLight.enabled = true;
					GetComponent<AudioSource> ().clip = flashLightOn;
					GetComponent<AudioSource> ().Play ();
				}
				else if (!_On)
				{
					_flashLight.enabled = false;
					GetComponent<AudioSource> ().clip = flashLightOff;
					GetComponent<AudioSource> ().Play ();
				}
					
			}	
		}
	}
}