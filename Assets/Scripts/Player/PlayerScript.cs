using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ZombieHunting
{
	public class PlayerScript : MonoBehaviour
	{
		private int _startingHealth = 100;

		public int health;

		public AudioClip hurtSound;
		public AudioClip deathSound;

		public bool isDead;
		public bool damaged;

		void Awake()
		{
			//setea la vida inicial en 100
			health = _startingHealth;
		}

		void Start()
		{
			//oculta el cursor
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = false;
		}

		/*metodo para cuando el player
		 * es atacado por un zombie
		 * toma un entero como parametro
		 * que le es pasado desde el 
		 * script del enemigo*/
		public void takeDamage (int amount)
		{
			GetComponent<AudioSource> ().clip = hurtSound;
			GetComponent<AudioSource> ().Play ();

			health -= amount;
		}

		public void Dead()
		{
			if (health <= 0 && !isDead)
			{
				isDead = true;

				GetComponent<AudioSource> ().clip = deathSound;
				GetComponent<AudioSource> ().Play ();
			}
		}

		/*metodo para cuando junta
		 * un botiquin*/
		public void Medicine()
		{
			health = health + 15;
			if (health > 100)
			{
				health = 100;
			}
		}

		/*los siguientes dos metodos
		 * son para setear como hijo
		 * del ascensor al player
		 * cuando entra para evitar
		 * que traspase el ascensor */
		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Elevator")
			{
				this.transform.SetParent (other.transform, true);
			}
		}

		void OnTriggerExit(Collider other)
		{
			if (other.gameObject.tag == "Elevator")
			{
				this.transform.SetParent (null);
			}
		}

		void Update()
		{
			Dead ();
		}
	}
}