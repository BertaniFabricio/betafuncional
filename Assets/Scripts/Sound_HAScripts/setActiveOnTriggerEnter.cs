﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ZombieHunting
{
	//NO CAMBIO NADA
	public class setActiveOnTriggerEnter : MonoBehaviour
	{
		public List<GameObject> objectsToSet = new List<GameObject>();

		private bool _active = true;

		private bool _selfDestroy = true;

		void OnTriggerEnter( Collider other )
		{
			if( other.tag == "Player" )
			{
				foreach( GameObject o in objectsToSet ) o.SetActive( _active );
				
				if( _selfDestroy ) Destroy ( this.gameObject );
			}
		}
	}
}