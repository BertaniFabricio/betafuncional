﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	public class allDoors : MonoBehaviour
	{
		//SAQUE TODAS LAS ANIMACIONES DE LAS PUERTAS Y LAS HICE POR CODIGO

		//determina los angulos de la puerta
		public float doorOpenAngle = 90.0f;
		public float doorCloseAngle = 0.0f;
		public float doorAnimSpeed = 2.0f;

		private Quaternion _doorOpen = Quaternion.identity;
		private Quaternion _doorClose = Quaternion.identity;

		private Transform _playerTrans = null;

		public bool doorStatus = false;
		private bool _doorGo = false;

		public AudioClip doorOpenSound;
		public AudioClip doorCloseSound;

		GameManager _gmScript;

		void Start()
		{
			doorStatus = false;

			_gmScript = GameObject.FindGameObjectWithTag ("GmGen").GetComponent<GameManager> ();

			//fija las rotaciones angulares en 0
			_doorOpen = Quaternion.Euler (0, doorOpenAngle, 0);
			_doorClose = Quaternion.Euler (0, doorCloseAngle, 0);

			//busca el transform del player para comparar cercania
			if (_gmScript.isIntro == false)
			{	
			_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			}
		}

		//corutina para abrir o cerrar la puerta, hay que pasarle una rotación destino como parametro
		public IEnumerator moveDoor(Quaternion dest)
		{
			_doorGo = true;//bool en true para activar

			while (Quaternion.Angle (transform.localRotation, dest) > 4.0f)
			{
				transform.localRotation = Quaternion.Slerp (transform.localRotation, dest, Time.deltaTime * doorAnimSpeed);
				yield return null;
			}

			doorStatus = !doorStatus;
			_doorGo = false;

			yield return null;
		}

		void Update()
		{
			if (_gmScript.isIntro == false)
			{	
				_playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
			}
			if (Input.GetKeyDown (KeyCode.E) && !_doorGo)
			{
				//Si presiona la tecla E y el bool es falso
				if (Vector3.Distance (_playerTrans.position, this.transform.position) < 2.5f)
				//y la distancia entre el player y la puerta es menor de 2.5f
				{
					if (doorStatus)//si este bool es positivo la puerta esta abierta y se cierra
					{
						StartCoroutine (this.moveDoor (_doorClose));
						GetComponent<AudioSource> ().clip = doorCloseSound;
						GetComponent<AudioSource> ().Play ();
					}
					else //de lo contrario el bool es negativo por lo que la puerta esta cerrada y se abre
					{
						StartCoroutine (this.moveDoor (_doorOpen));
						GetComponent<AudioSource> ().clip = doorOpenSound;
						GetComponent<AudioSource> ().Play ();
					}		
				}
			}
		}
	}
}