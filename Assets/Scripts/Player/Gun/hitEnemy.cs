﻿using UnityEngine;
using System.Collections;

namespace ZombieHunting
{
	/*METODO PARA INTANCIAR PARTICULA DE SANGRE EN ENEMIGOS
	 *Y LLAMA AL METODO QUE LE RESTA VIDA AL ENEMIGO*/

	public class hitEnemy : MonoBehaviour //hereda del script del enemigo
	{

		enemyScript _enemyScript;

		public GameObject bloodParticle;

		void Awake()
		{
			_enemyScript = GetComponent<enemyScript> ();
		}

		public void rayHitEnemy(RaycastHit hit, bool isHead)
		{
			Vector3 pos = hit.point;
			Quaternion rot = Quaternion.FromToRotation (Vector3.up, hit.normal);
			GameObject Particle = Instantiate (bloodParticle, pos, rot) as GameObject;
			Particle.transform.parent = this.transform;
			if (!isHead)
			{
				_enemyScript.takeDamage ();
			}

			if (isHead)
			{
				//Debug.Log ("Capocha");
				_enemyScript.headShot ();
			}
		}
	}
}