﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson; //<--
//necesario para acceder al script del firstpersoncontroller -->

namespace ZombieHunting
{
	/*ESTE GAME MANAGER ES PARA CUANDO
	 * EL PLAYER PIERDE */
	public class GameManagerGameOver : MonoBehaviour
	{
		public GameObject GameOverDisplay; //pantalla que sale cuando pierde

		FirstPersonController _fps;
		GunScript _gunScript;
		PlayerScript _playerScript;

		void Start()
		{
			_fps = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
			_playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			_gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<GunScript> ();
			GameOverDisplay.SetActive (false);
		}

		IEnumerator timeToRestart()
		{
			yield return new WaitForSeconds (8.0f);
			SceneManager.LoadScene ("Hospital");

			yield return null;
		}

		void Update()
		{
			if (_playerScript.health <= 0)
			{
				GameOverDisplay.SetActive (true); //activa la pantalla
				_fps.enabled = false; //<--
				/*desactiva el firstpersoncontroller para evitar que
				 * se siga moviendo el mouse --> */
				_gunScript.enabled = false; //<--
				/*desactiva el script del arma para evitar
				 * que pueda seguir disparando --> */
				StartCoroutine (timeToRestart ());
			}
		}
	}
}